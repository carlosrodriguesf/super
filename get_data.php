<?php
require_once __DIR__ . "/vendor/autoload.php";

$arr = \Symfony\Component\Yaml\Yaml::parse(file_get_contents(__DIR__ . "/connections.yml"));

if (empty($arr) || empty($arr['connections'])) {
    throw new Exception("No connections found!");
}

$connections = array_map(function ($arrConnection, $key) {
    return \Lib\Connection::fromArray($arrConnection + ['name' => $key]);
}, $arr['connections'], array_keys($arr['connections']));

$result = [];

/**
 * @var \Lib\Connection $connection
 */
foreach ($connections as $connection) {
    $result[$connection->getName()] = $connection->getProcesses();
}

header('Content-Type: application/json');
http_response_code(200);
echo json_encode($result, JSON_PRETTY_PRINT);