<?php

namespace Lib;

use Supervisor\Supervisor;

class Connection
{
    private static $count = 0;

    /**
     * @var string
     */
    private $name;
    /**
     * @var string
     */
    private $host;

    /**
     * @var string
     */
    private $port;

    /**
     * @var string
     */
    private $user;

    /**
     * @var string
     */
    private $pass;

    /**
     * @var Supervisor
     */
    private $supervisor;

    /**
     * Connection constructor.
     * @param string $host
     * @param string $port
     * @param string $user
     * @param string $pass
     */
    public function __construct($name, $host, $port, $user, $pass)
    {
        $this->name = $name;
        $this->host = $host;
        $this->port = $port;
        $this->user = $user;
        $this->pass = $pass;

        $this->connect();
    }

    private function connect()
    {
        $messageFactory = new \Lib\MessageFactory();
        $httpClient = new \Lib\HttpClient(['auth' => [$this->user, $this->pass]]);
        $xmlRpcHttpAdapterTransport = new \fXmlRpc\Transport\HttpAdapterTransport($messageFactory, $httpClient);
        $xmlRpcClient = new \fXmlRpc\Client('http://' . $this->host . ':' . $this->port . '/RPC2', $xmlRpcHttpAdapterTransport);
        $xmlRpcConnector = new \Supervisor\Connector\XmlRpc($xmlRpcClient);

        $this->supervisor = new \Supervisor\Supervisor($xmlRpcConnector);
    }

    /**
     * @return int
     */
    public static function getCount(): int
    {
        return self::$count;
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @return string
     */
    public function getHost(): string
    {
        return $this->host;
    }

    /**
     * @return string
     */
    public function getPort(): string
    {
        return $this->port;
    }

    /**
     * @return string
     */
    public function getUser(): string
    {
        return $this->user;
    }

    /**
     * @return string
     */
    public function getPass(): string
    {
        return $this->pass;
    }

    /**
     * @return Supervisor
     */
    public function getSupervisor(): Supervisor
    {
        return $this->supervisor;
    }

    /**
     * @return array
     */
    public function getProcesses()
    {
        return $this->supervisor->getAllProcessInfo();
    }

    /**
     * @param array $arr
     * @return Connection
     */
    public static function fromArray(array $arr)
    {
        return new self(
            $arr['display_name'] ?? $arr['name'] ?? "connection#" . (++self::$count),
            $arr['host'],
            $arr['port'],
            $arr['user'],
            $arr['pass']
        );
    }
}